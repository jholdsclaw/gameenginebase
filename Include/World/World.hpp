#pragma once

#ifndef WORLD_HPP
#define WORLD_HPP

#include <Resources/ResourceHolder.hpp>
#include <Resources/ResourceIdentifiers.hpp>
#include <Resources/SceneNode.hpp>
#include <Resources/SpriteNode.hpp>
#include <Entities/Avatar.hpp>
#include <Input/CommandQueue.hpp>
#include <Input/Command.hpp>

#include <Utility/ResourcePath.hpp>

#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/Texture.hpp>

#include <array>


// Forward declaration
namespace sf
{
	class RenderWindow;
}

class World : private sf::NonCopyable
{
	public:
		explicit							World(sf::RenderWindow& window);
		void								update(sf::Time dt);
		void								draw();

		CommandQueue&						getCommandQueue();


	private:
		void								loadTextures();
		void								buildScene();
		void								adaptPlayerPosition();
		void								adaptPlayerVelocity();


	private:
		enum Layer
		{
			Background,
			Air,
			LayerCount
		};


	private:
		sf::RenderWindow&					mWindow;
		sf::View							mWorldView;
		TextureHolder						mTextures;

		SceneNode							mSceneGraph;
		std::array<SceneNode*, LayerCount>	mSceneLayers;
		CommandQueue						mCommandQueue;

		sf::FloatRect						mWorldBounds;
		sf::Vector2f						mSpawnPosition;
		float								mScrollSpeed;
		Avatar*								mPlayerAvatar;
};

#endif // WORLD_HPP
