#pragma once

#ifndef AVATAR_HPP
#define AVATAR_HPP

#include <Entities/Entity.hpp>
#include <Resources/ResourceIdentifiers.hpp>

#include <SFML/Graphics/Sprite.hpp>


class Avatar : public Entity
{
	public:
		enum Type
		{
			Eagle,
			Raptor,
		};


	public:
								Avatar(Type type, const TextureHolder& textures);

		virtual void			drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
		virtual unsigned int	getCategory() const;


	private:
		Type					mType;
		sf::Sprite				mSprite;
};

#endif // AVATAR_HPP
