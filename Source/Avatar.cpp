#include <Entities/Avatar.hpp>
#include <Resources/ResourceHolder.hpp>
#include <Utility/Utility.hpp>

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>


Textures::ID toTextureID(Avatar::Type type)
{
	switch (type)
	{
		case Avatar::Eagle:
			return Textures::Eagle;

		case Avatar::Raptor:
			return Textures::Raptor;
	}
}

Avatar::Avatar(Type type, const TextureHolder& textures)
: mType(type)
, mSprite(textures.get(toTextureID(type)))
{
	centerOrigin(mSprite);
}

void Avatar::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(mSprite, states);
}

// TODO: Shouldn't this be somehow refactored into the input definitions?
unsigned int Avatar::getCategory() const
{
	switch (mType)
	{
	case Eagle:
		return Category::PlayerAvatar;

	default:
		return Category::EnemyAvatar;
	}
}
